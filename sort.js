function sorts() {
  const fruits = ["Banana", "Orange", "Apple", "Mango"];
  fruits.sort();
  const v = [40, 100, 1, 5, 25, 10];
  v.sort(function (a, b) {
    return a - b;
  });
  console.log(fruits);
  console.log("asc");
  console.log(v);
  console.log("desc");
  v.reverse();
  console.log(v);
}
sorts();
